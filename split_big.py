#!/usr/bin/env python
"""
Split hdf5 (big) file
"""
from __future__ import print_function
import os
from parser import get_args_split as parser
import msg
import hdf5
import check
from combine_big import load
from split import generate_filelist
from split import save_filelist

if __name__ == '__main__':

    msg.box("HDF5 MANIPULATOR: SPLIT")

    args = parser()
    print(args.group)
    data_original = load(args.input, group_path=args.group)
    data = None
    if args.group:
        data = data_original[args.group]


    filelist = generate_filelist(
        args.prefix or os.path.splitext(args.input)[0],
        check.get_size(data), int(args.size))

    print("\nSaving output files:\n")
    print(filelist)

    for f, r in filelist.items():
        msg.list_fileinfo(f, r)
        hdf5.save_subset_big(f, data, r[0], r[1])

    if args.filelist:
        save_filelist(args.filelist, filelist.keys())

    data_original.close()

    msg.info("Done")
